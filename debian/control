Source: tpm2-pytss
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Claudius Heine <ch@denx.de>
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 libtss2-dev,
 pybuild-plugin-pyproject,
 python3-all:any,
 python3-all-dev,
 python3-asn1crypto,
 python3-cffi (>= 1.0.0),
 python3-cryptography (>= 3.0),
 python3-doc,
 python3-myst-parser,
 python3-packaging,
 python3-pkgconfig,
 python3-pycparser,
 python3-pytest-cov <!nocheck>,
 python3-pytest-forked <!nocheck>,
 python3-pytest <!nocheck>,
 python3-pytest-xdist <!nocheck>,
 python3-setuptools,
 python3-setuptools-scm,
 python3-sphinx <!nodoc>,
 python3-sphinx-rtd-theme <!nodoc>,
 swtpm <!nocheck>,
Standards-Version: 4.6.2
Homepage: https://github.com/tpm2-software/tpm2-pytss
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/python-team/packages/tpm2-pytss.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/tpm2-pytss

Package: python-tpm2-pytss-doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Section: doc
Build-Profiles: <!nodoc>
Description: Provides Python bindings for a number of APIs for the TPM2 TSS (documentation)
 TPM2 TSS Python bindings for Enhanced System API (ESYS), Feature API (FAPI),
 Marshaling (MU), TCTI Loader (TCTILdr) and RC Decoding (rcdecode) libraries.
 It also contains utility methods for wrapping keys to TPM 2.0 data structures
 for importation into the TPM, unwrapping keys and exporting them from the TPM,
 TPM-less makecredential command and name calculations, TSS2 PEM Key format
 support, importing Keys from PEM, DER and SSH formats, conversion from
 tpm2-tools based command line strings and loading tpm2-tools context files.
 .
 This package contains the documentation for TPM2 PyTSS.

Package: python3-tpm2-pytss
Architecture: any
Depends:
 python3-asn1crypto,
 python3-cffi (>= 1.0.0),
 python3-cryptography (>= 3.0),
 python3-packaging,
 python3-yaml,
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Suggests: python-tpm2-pytss-doc
Description: Provides Python bindings for a number of APIs for the TPM2 TSS (Python 3)
 TPM2 TSS Python bindings for Enhanced System API (ESYS), Feature API (FAPI),
 Marshaling (MU), TCTI Loader (TCTILdr) and RC Decoding (rcdecode) libraries.
 It also contains utility methods for wrapping keys to TPM 2.0 data structures
 for importation into the TPM, unwrapping keys and exporting them from the TPM,
 TPM-less makecredential command and name calculations, TSS2 PEM Key format
 support, importing Keys from PEM, DER and SSH formats, conversion from
 tpm2-tools based command line strings and loading tpm2-tools context files.
 .
 This package installs the library for Python 3.
